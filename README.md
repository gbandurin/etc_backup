## Bash script for backuping /etc directory
It will make a tarball archive of etc directory only in nighttime, and delete old archives whenever you call it with REMOVE_AFTER variable.

# Typical usage
To wipe files older, than 30 days, start this script with following command:
```
env REMOVE_AFTER=30 ./etc_backup.sh
```
Howewer for archieving a directory, you need to start this script between two and three hours after midnight. To do so, add a cronjob:
```
15 2 * * * path/to/etc_backup.sh
```

## License
Copyright (c) 2021, authored by George Bandurin

You can use this configuration without any restrictions.
This file comes with no warranty and no responsibilities for any consequences caused by this
configuration, or misconfiguration of this configuration.

