#!/bin/bash
# Pass variable REMOVE_AFTER=<days> to automatically wipe 
# backups older, than number of <days>. For example:
# start this script with 'env REMOVE_AFTER=30 ./etc_backup.sh'
# to wipe files older, than 30 days.

# == Variables ==
today=$(TZ=":Europe/Moscow" date +'%Y-%m-%d')	#date
source_dir=/etc					#What to backup
target_dir=/var/lib/backups			#Where to put backup archive
log_file=/var/log/etc_backup.log		#Place where the log belongs
now_h=$(TZ=":Europe/Moscow" date +"%H")		#What hour is it now?

# == Functions ==
# Func to remove old backups
# Usage: remove_old <path> <days>
remove_old () {
	find $1/etc-backup-*.tar.gz -mtime +$2 -type f -delete -print
}

# Func to backup with tar
# Usage: tar_backup <source> <target> <date>
tar_backup () {
	tar cvzf $2/etc-backup-$3.tar.gz $1
}

# Func logger
# Usage: log_it <process_name>
log_it () {
	logger -s -t $(basename $1)
}

# == Working body ==
# Checking if root
if [ "$EUID" -ne 0 ]; then
	echo "Error: invoking script file as regular user. Please run as root." | log_it $0
	echo "Backup failed. Job will now exit." | log_it $0
	exit
else
	if [ -e "$log_file" ]; then :; else touch $log_file; fi
	echo "=====================================================" | tee -a $log_file
	echo "$(date +'%d-%b-%Y %R:%S')" | tee -a $log_file
	echo "-----------------------------------------------------" | tee -a $log_file
	echo "Invoking $0 script file." | tee -a $log_file
	echo "Root privileges: GRANTED" | tee -a $log_file
fi

# Check if target dir exists
if [ -e "$target_dir" ]; then
	# Wipe old backups
	if [ -n "$REMOVE_AFTER" ]; then 
		echo "Following files older than $REMOVE_AFTER days will be removed:" | tee -a $log_file
		remove_old $target_dir $REMOVE_AFTER 2>&1 | tee -a $log_file
		echo "Remove routine finished." | tee -a $log_file
	else
		echo "REMOVE_AFTER variable not set. Will not delete old backups." | tee -a $log_file
	fi
else
	mkdir -p $target_dir
	echo "$target_dir was missing; now it's created." | tee -a $log_file
fi

# Archieving /etc dir if it's 2 a.m.
if [[ "$now_h" -ge 2 && "$now_h" -lt 3 ]]; then
	# Avoiding pipe to work if Error level>0
	set -o pipefail 
	echo "++++++++++ Starting backup routine..." | tee -a $log_file
	tar_backup $source_dir $target_dir $today 2>&1 | tee -a $log_file
	if [ "$?" -gt 0 ]; then 
		echo "Archieving of file encountered irrecoverable error. Job failed." | tee -a $log_file
	else
		echo "++++++++++ Backup finished!" | tee -a $log_file
	fi
else
	echo "Error running backup. It's not 2 a.m. in my local timezone (UTC+3) now." | tee -a $log_file
fi

